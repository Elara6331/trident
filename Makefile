all: action.go config.go audio.go logging.go main.go plugins.go
	go build

install: trident trident.example.toml deepspeech.pbmm deepspeech.scorer
	rm -rf /opt/trident/
	mkdir -p /opt/trident/
	install -Dm755 trident /opt/trident/trident
	cp model.pbmm /opt/trident/
	cp model.scorer /opt/trident/
	cp trident.example.toml /opt/trident/trident.toml
	cp -r plugins /opt/trident/

.PHONY: all install