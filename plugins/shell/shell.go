/*
 *   Copyright (C) 2021 Arsen Musayelyan
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

// shell is a Trident plugin that runs a command in a shell
package shell

import (
	"os"
	"os/exec"
	"trident"
)

func RunPlugin(program string, data map[string]interface{}) {
	// Attempt to get shell from config, asserting as string
	shell, ok := data["shell"].(string)
	// If unsuccessful
	if !ok {
		// Set shell to default (/bin/sh)
		shell = "/bin/sh"
	}
	sayOutput, ok := data["sayOutput"].(bool)
	if !ok {
		sayOutput = false
	}
	// Create command using configured shell or default (/bin/sh)
	cmd := exec.Command(shell, "-c", program)
	// Set command environment to system environment
	cmd.Env = os.Environ()
	// Set command's standard error to system standard error
	cmd.Stderr = os.Stderr
	// Run command, ignoring error
	output, _ := cmd.Output()
	if sayOutput {
		trident.Say(string(output))
	}
}
