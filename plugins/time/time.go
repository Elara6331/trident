/*
 *   Copyright (C) 2021 Arsen Musayelyan
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

// time is a Trident plugin that says the current time using a synthesized voice
package time

import (
	"time"
	"trident"
)

func RunPlugin(_ string, _ map[string]interface{}) {
	// Format time in a way the synthesized voice will understand
	formattedTime := time.Now().Format("3:04 PM")
	// Say formatted time using voice
	trident.Say(formattedTime)
}
