# Trident

Personal voice assistant written in go using DeepSpeech. This was mainly created for personal use, so there may be some edge cases unaccounted for.

---

#### Prerequisites

The prerequisites for trident are:
- libdeepspeech and DeepSpeech models (speech to text)
- flite (text to speech)
- go

`libdeepspeech` along with its models can be found in [its github releases](https://github.com/mozilla/DeepSpeech/releases/). Be sure to download the `native_client` tarball for your platform and the `.pbmm` and `.scorer` files.

`flite` can be installed via your distribution's repositories:
- Debian/Ubuntu: `sudo apt install flite-dev`
- Fedora: `sudo dnf install flite-devel`
- Arch: `sudo pacman -S flite festival-us`
### Installation

Move the previously downloaded models (`.pbmm` and `.scorer`) into this repo as `deepspeech.pbmm` and `deepspeech.scorer`. Then, follow the next steps.

To build trident, run:
```shell
make
```
After that, install by running:
```shell
sudo make install
```

### Customization

To change the activation tone, replace the `activate.wav` file with any valid wav file and rebuild. The file will be embedded in the executable via `//go:embed`.

Trident has a config file called `trident.toml`. It will first look for it in `<user config directory>/trident.toml` (config directory as defined by `os.UserConfigDir()`), then it will look for it in the same directory as the executable.