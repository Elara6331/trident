module gitea.arsenm.dev/Arsen6331/trident

go 1.16

require (
	github.com/asticode/go-astideepspeech v0.10.0
	github.com/gen2brain/flite-go v0.0.0-20170519100317-f4df2119132c
	github.com/gen2brain/malgo v0.10.29
	github.com/pelletier/go-toml v1.9.0
	github.com/rs/zerolog v1.21.0
	github.com/spf13/pflag v1.0.5
	github.com/stretchr/testify v1.7.0 // indirect
	github.com/traefik/yaegi v0.9.17
	github.com/youpy/go-wav v0.1.0
)
