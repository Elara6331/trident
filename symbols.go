/*
 *   Copyright (C) 2021 Arsen Musayelyan
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package main

import (
	"github.com/gen2brain/flite-go"
	"github.com/traefik/yaegi/interp"
	"reflect"
)

// Create custom package for trident
var tridentSymbols = interp.Exports{"trident": {
	"Say":          reflect.ValueOf(Say),
	"SayWithVoice": reflect.ValueOf(SayWithVoice),
}}

// Function to say text using mimic text-to-speech
func Say(text string) {
	fliteVoice, _ := flite.VoiceSelect("slt")
	flite.TextToSpeech(text, fliteVoice, "play")
}

func SayWithVoice(text, voice string) error {
	fliteVoice, err := flite.VoiceSelect(voice)
	if err != nil {
		return err
	}
	flite.TextToSpeech(text, fliteVoice, "play")
	return nil
}
