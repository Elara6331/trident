/*
 *   Copyright (C) 2021 Arsen Musayelyan
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package main

import (
	"github.com/pelletier/go-toml"
	"go/build"
	"os"
	"path/filepath"
	"time"
)

// Config stores the root of the TOML config
type Config struct {
	ActivationPhrase string        `toml:"activationPhrase"`
	ActivationTime   time.Duration `toml:"activationTime"`
	IPCEnabled       bool          `toml:"ipcEnabled"`
	Actions          []Action      `toml:"action"`
}

// Action stores voice-activated actions in config
type Action struct {
	Name   string                 `toml:"name"`
	Type   string                 `toml:"type"`
	Phrase string                 `toml:"phrase"`
	Input  string                 `toml:"input,omitempty"`
	Data   map[string]interface{} `toml:"data,omitempty"`
}

// getConfig parses and returns a TOML config
func getConfig(path string) (*Config, error) {
	// Open file at given path
	file, err := os.Open(filepath.Clean(path))
	if err != nil {
		return nil, err
	}
	// Create new nil struct
	var out Config
	// Create new TOML decoder reading from file
	dec := toml.NewDecoder(file)
	// Decode contents of file to struct
	err = dec.Decode(&out)
	if err != nil {
		return nil, err
	}
	return &out, nil
}

// Get all relevant paths
func configEnv() (gopath, configDir, execDir, confPath string) {
	// Get path to currently running executable
	execPath, err := os.Executable()
	if err != nil {
		log.Fatal().Err(err).Msg("Error getting executable path")
	}
	// Get current user's configuration directory
	confDir, err := os.UserConfigDir()
	if err != nil {
		log.Fatal().Err(err).Msg("Error getting config directory")
	}
	// Try to get GOPATH environment variable
	gopath = os.Getenv("GOPATH")
	// If not set
	if gopath == "" {
		// Use default GOPATH
		gopath = build.Default.GOPATH
	}
	// Set configuration directory to <user config dir>/trident
	configDir = filepath.Join(confDir, "trident")
	// Set executable directory to the directory of execPath
	execDir = filepath.Dir(execPath)
	// Set config path to file trident.toml inside config directory
	confPath = filepath.Join(configDir, "trident.toml")
	// If file is not accessible
	if _, err := os.Stat(confPath); err != nil {
		// Use config in same executable directory
		confPath = filepath.Join(execDir, "trident.toml")
	}
	// Return all variables
	return
}
